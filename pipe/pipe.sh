#!/bin/bash
#
# artifactory-generic-upload pipe
#
# Required globals:
#   ARTIFACTORY_URL
#   ARTIFACTORY_USER
#   ARTIFACTORY_PASSWORD
#   FILE_SPEC
#   SOURCE_PATH
#   TARGET_PATH
#
# Optional globals:
#   JFROG_CLI_TEMP_DIR
#   JFROG_CLI_HOME_DIR
#   FILE_SPEC_PATH
#   COLLECT_ENV
#   COLLECT_GIT_INFO
#   COLLECT_BUILD_INFO
#   EXTRA_ARGS
#   EXTRA_BAG_ARGS
#   BUILD_NAME
#

source "$(dirname "$0")/common.sh"

## Enable debug mode.
DEBUG_ARGS=
enable_debug() {
  if [[ "${DEBUG}" == "true" ]]; then
    info "Enabling debug mode."
    set -x
    DEBUG_ARGS="--verbose"
    export JFROG_CLI_LOG_LEVEL="DEBUG"
  fi
}

info "Starting pipe execution..."

# required parameters
ARTIFACTORY_URL=${ARTIFACTORY_URL:?'ARTIFACTORY_URL variable missing.'}
ARTIFACTORY_USER=${ARTIFACTORY_USER:?'ARTIFACTORY_USER variable missing.'}
ARTIFACTORY_PASSWORD=${ARTIFACTORY_PASSWORD:?'ARTIFACTORY_PASSWORD variable missing.'}
FILE_SPEC=${FILE_SPEC:?'FILE_SPEC variable missing.'}
if [[ "${FILE_SPEC}" != "true" ]]; then
  SOURCE_PATH=${SOURCE_PATH:?'SOURCE_PATH variable missing.'}
  TARGET_PATH=${TARGET_PATH:?'TARGET_PATH variable missing.'}
fi

# optional parameters
BUILD_NAME=${BUILD_NAME:=$BITBUCKET_REPO_OWNER-$BITBUCKET_REPO_SLUG-$BITBUCKET_BRANCH}
JFROG_CLI_TEMP_DIR=${JFROG_CLI_TEMP_DIR:="."}
JFROG_CLI_HOME_DIR=${JFROG_CLI_HOME_DIR:="."}
FILE_SPEC_PATH=${FILE_SPEC_PATH:="./spec.json"}
COLLECT_ENV=${COLLECT_ENV:="true"}
COLLECT_GIT_INFO=${COLLECT_GIT_INFO:="true"}
COLLECT_BUILD_INFO=${COLLECT_BUILD_INFO:="true"}
EXTRA_ARGS=${EXTRA_ARGS:=""}
EXTRA_BAG_ARGS=${EXTRA_BAG_ARGS:=""}
DEBUG=${DEBUG:="false"}

# Set the environment variable
export JFROG_CLI_TEMP_DIR=$JFROG_CLI_TEMP_DIR
export JFROG_CLI_HOME_DIR=$JFROG_CLI_HOME_DIR
export BUILD_URL="https://bitbucket.org/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/addon/pipelines/home#!/results/${BITBUCKET_BUILD_NUMBER}"

debug "build name is ${BUILD_NAME}"
debug "build number is ${BITBUCKET_BUILD_NUMBER}"

check_status() {
if [[ "${status}" -eq 0 ]]; then
  success "Successfully uploaded artifacts from Artifactory."
else
  fail "Failed to upload artifacts from Artifactory."
fi
}

# Configure Artifactory instance with JFrog CLI
run jfrog rt config --url=$ARTIFACTORY_URL --user=$ARTIFACTORY_USER --password=$ARTIFACTORY_PASSWORD --interactive=false --enc-password=false
check_status

# Run the Download command
if [[ "${FILE_SPEC}" == "true" ]]; then
  debug "Uploading artifacts using file spec provided in ${FILE_SPEC_PATH}"
  run jfrog rt u --spec $FILE_SPEC_PATH --build-name=$BUILD_NAME --build-number=$BITBUCKET_BUILD_NUMBER $EXTRA_ARGS
  check_status
else
  run jfrog rt u $SOURCE_PATH $TARGET_PATH --build-name=$BUILD_NAME --build-number=$BITBUCKET_BUILD_NUMBER $EXTRA_ARGS
  check_status
fi
# Capture environment variables for build information
if [[ "${COLLECT_ENV}" == "true" ]]; then
   info "Capturing environment variables"
   run jfrog rt bce $BUILD_NAME $BITBUCKET_BUILD_NUMBER
   check_status
fi

# Collecting Information from Git
if [[ "${COLLECT_GIT_INFO}" == "true" ]]; then
   info "Collecting Information from Git"
   run jfrog rt bag $BUILD_NAME $BITBUCKET_BUILD_NUMBER $EXTRA_BAG_ARGS
   check_status
fi

# Publish build information to Artifactory
if [[ "${COLLECT_BUILD_INFO}" == "true" ]]; then
   info "Capturing build information"
   run jfrog rt bp $BUILD_NAME $BITBUCKET_BUILD_NUMBER --build-url="${BUILD_URL}"
   check_status
fi

