# Bitbucket Pipelines Pipe: Artifactory Generic Upload

This pipe uploads an artifact to an [JFrog Artifactory](https://jfrog.com/artifactory/?utm_source=BitbucketPipes&utm_medium=UX) repository.

By default, this pipe will also capture build-info and publish it to Artifactory as metadata associated with the uploaded file.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: JfrogDev/artifactory-generic-upload:0.3.2
  variables:
    ARTIFACTORY_URL: '<string>'
    ARTIFACTORY_USER: '<string>'
    ARTIFACTORY_PASSWORD: '<string>'
    FILE_SPEC: '<boolean>'
    SOURCE_PATH: '<string>'
    TARGET_PATH: '<string>'
    # BUILD_NAME: '<string>' # Optional.
    # JFROG_CLI_TEMP_DIR: '<string>' # Optional.
    # JFROG_CLI_HOME_DIR: '<string>' # Optional.
    # COLLECT_ENV: '<boolean>' # Optional.
    # COLLECT_GIT_INFO: '<boolean>' # Optional.
    # COLLECT_BUILD_INFO: '<boolean>' # Optional.
    # FILE_SPEC_PATH: '<string>' # Optional.
    # FOLDER: '<string>' # Optional.
    # EXTRA_ARGS: '<string>' # Optional.
    # EXTRA_BAG_ARGS: '<string>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables

| Variable        | Usage                                     |
| --------------  | ----------------------------------------- |
| ARTIFACTORY_URL (\*)  | The JFrog Artifactory URL . |
| ARTIFACTORY_USER (\*)  | Artifactory User with permission to upload artifacts. |
| ARTIFACTORY_PASSWORD (\*)  | Password for Artifactory User. |
| SOURCE_PATH (\*)  | Specifies the local file system path to artifacts to be uploaded to Artifactory. You can specify multiple artifacts by using wildcards or a regular expression as designated by the --regexp command option. |
| TARGET_PATH (\*)  | Specifies the target path in Artifactory in the following format: [repository_name]/[repository_path] |
| FILE_SPEC (\*)    | Set to _true_ to use file spec to download artifacts. Default: `false`. |
| FILE_SPEC_PATH (\*)  | Specifes the local file system path to a file spec. For more details, please refer to [Using File Specs](https://www.jfrog.com/confluence/display/CLI/CLI+for+JFrog+Artifactory#CLIforJFrogArtifactory-UsingFileSpecs). |
| BUILD_NAME | Build Name. Default: `$BITBUCKET_REPO_OWNER-$BITBUCKET_REPO_SLUG-$BITBUCKET_BRANCH` |
| JFROG_CLI_TEMP_DIR  | Defines the JFrog CLI temp directory. Default: `.` |
| JFROG_CLI_HOME_DIR  | Defines the JFrog CLI home directory.Default: `.` |
| COLLECT_ENV  | This flag is used to collect environment variables and attach them to a build. Default: `true` |
| COLLECT_GIT_INFO  | This flag is used to [collects the Git](https://www.jfrog.com/confluence/display/CLI/CLI+for+JFrog+Artifactory#CLIforJFrogArtifactory-BuildIntegration-AddingGitInformation) revision and URL from the local .git directory and adds it to the build-info. Default: `true` |
| COLLECT_BUILD_INFO  | This flag is used to publish build info to Artifactory. Default: `true` |
| FOLDER          | A folder containing the `package.json` file. Default to the current directory: `.` |
| EXTRA_ARGS      | Extra arguments to be passed to the JFrog CLI command (see [JFrog CLI docs](https://www.jfrog.com/confluence/display/CLI/CLI+for+JFrog+Artifactory) for more details). Defaults to unset. |
| EXTRA_BAG_ARGS  | Extra arguments to be passed to the build-add-git (bag) JFrog CLI command (see [JFrog CLI docs](https://www.jfrog.com/confluence/display/CLI/CLI+for+JFrog+Artifactory#CLIforJFrogArtifactory-BuildIntegration-AddingGitInformation) for more details). Defaults to unset. |
| DEBUG           | Set to _true_ to output additional debug information. Default: `false`. |

_(\*) = required variable._

## Details
This pipe is used to upload files to [Artifactory](https://jfrog.com/artifactory/?utm_source=BitbucketPipes&utm_medium=UX).

## Prerequisites
[JFrog Artifactory](https://jfrog.com/artifactory/?utm_source=BitbucketPipes&utm_medium=UX) details are necessary to use this pipe.

- Add the Credentials as a [secured environment variable](https://confluence.atlassian.com/x/0CVbLw#Environmentvariables-Securedvariables) in Bitbucket Pipelines.
- [Artifactory Repository](https://www.jfrog.com/confluence/display/RTF/Configuring+Repositories) is required.

## Examples

### Basic example 
Upload artifact(s) to [Artifactory Repository](https://www.jfrog.com/confluence/display/RTF/Configuring+Repositories).

Following example is used to upload all artifacts with `.zip` extension from `./generic` directory in current worksapce to `generic-local` repository in Artifactory.
```yaml
script:
  - pipe: JfrogDev/artifactory-generic-upload:0.3.2
    variables:
      ARTIFACTORY_URL: '<string>'
      ARTIFACTORY_USER: ${ARTIFACTORY_USER}
      ARTIFACTORY_PASSWORD: ${ARTIFACTORY_PASSWORD}
      FILE_SPEC: 'false'
      SOURCE_PATH: 'generic/*.zip'
      TARGET_PATH: 'generic-local/'
```

### Advanced example 
Upload artifacts to [Artifactory Repository](https://www.jfrog.com/confluence/display/RTF/Configuring+Repositories) using file spec provided in $FILE_SPEC_PATH.

```yaml
script:
  - pipe: JfrogDev/artifactory-generic-upload:0.3.2
    variables:
      ARTIFACTORY_URL: '<string>'
      ARTIFACTORY_USER: ${ARTIFACTORY_USER}
      ARTIFACTORY_PASSWORD: ${ARTIFACTORY_PASSWORD}
      FILE_SPEC: 'true'
      FILE_SPEC_PATH: './spec.json'
      EXTRA_ARGS: '--props="test=pass"'
```

_spec.json_ file example:
```json
{
  "files": [
    {
      "pattern": "*.zip",
      "target": "$ART_REPO_NAME/"
    }
  ]
}
```

## Support
If you'd like help with this pipe, or you have an issue or feature request, [let us know on](https://bitbucket.org/JfrogDev/artifactory-generic-upload/).

If you're reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License
Apache 2.0 licensed, see [LICENSE](LICENSE) file.

