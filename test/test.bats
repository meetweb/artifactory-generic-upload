#!/usr/bin/env bats
#
# Test Artifactory Generic Upload
#

set -e

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/artifactory-generic-download"}
  run docker build -t ${DOCKER_IMAGE} .
}

@test "Upload Artifact to Artifactory" {

  # execute tests
  run docker run \
    -e ARTIFACTORY_URL=$ARTIFACTORY_URL \
    -e ARTIFACTORY_USER=$ARTIFACTORY_USER \
    -e ARTIFACTORY_PASSWORD=$ARTIFACTORY_PASSWORD \
    -e FILE_SPEC="false" \
    -e SOURCE_PATH="test/*.zip" \
    -e TARGET_PATH="pipe-generic/" \
    -e BUILD_NAME=$BITBUCKET_REPO_OWNER-$BITBUCKET_REPO_SLUG-$BITBUCKET_BRANCH \
    -e BITBUCKET_BUILD_NUMBER=$BITBUCKET_BUILD_NUMBER \
    -e DEBUG="true" \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
  ${DOCKER_IMAGE}

  echo ${output}

  [[ "${status}" == "0" ]]

}


@test "Upload Artifact to Artifactory using file spec" {

  # execute tests
  run docker run \
    -e ARTIFACTORY_URL=$ARTIFACTORY_URL \
    -e ARTIFACTORY_USER=$ARTIFACTORY_USER \
    -e ARTIFACTORY_PASSWORD=$ARTIFACTORY_PASSWORD \
    -e FILE_SPEC="true" \
    -e FILE_SPEC_PATH="./test/spec.json" \
    -e BUILD_NAME=$BITBUCKET_REPO_OWNER-$BITBUCKET_REPO_SLUG-$BITBUCKET_BRANCH \
    -e BITBUCKET_BUILD_NUMBER=$BITBUCKET_BUILD_NUMBER \
    -e DEBUG="true" \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
  ${DOCKER_IMAGE}

  echo ${output}

  [[ "${status}" == "0" ]]

}
